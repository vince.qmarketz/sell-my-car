export const FIELD_TIME = [
  "9 AM",
  "10 AM",
  "11 AM",
  "1 PM",
  "2 PM",
  "3 PM",
  "4 PM",
];
export const FIELD_LOCATION = ["Loc 1", "Loc 2", "Loc 3", "Loc 4"];
