//array of data objects

const testimonialsData = [
  {
    name: "Miguel",
    testimonial: `“Some short copy here for testimonial. Vesting period influencer
        learning curve. Social media disruptive product management business
        plan”`,
    src: "images/testimonials/testimonial0.png",
  },
  {
    name: "Sample1",
    testimonial: `“Lorem ipsum dolor sit amet,
    consectetur adipiscing elit. Mauris ultricies
    pharetra ultrices. Sed ac facilisis dolor.
    In vestibulum mi non lorem consectetur tincidunt.”`,
    src: "images/testimonials/testimonial1.png",
  },
  {
    name: "Sample2",
    testimonial: `“Lorem ipsum dolor sit amet,
    consectetur adipiscing elit. Mauris ultricies
    pharetra ultrices. Sed ac facilisis dolor.
    In vestibulum mi non lorem consectetur tincidunt.”`,
    src: "images/testimonials/testimonial2.png",
  },
];
export default testimonialsData;
