const theme = {
  overrides: {
    // Style sheet name ⚛️
    // MuiList: {
    //   root: {
    //   },
    // },
  },
  palette: {
    primary: {
      light: "#545678",
      main: "#2A2D4C",
      dark: "#020224",
      contrastText: "#fff",
      background: "rgba(255,147,44,.6)",
    },
    secondary: {
      light: "#9fd497",
      main: "#6FA269",
      dark: "#42743d",
      contrastText: "#fff",
    },
  },
  typography: {
    fontFamily: `'Source Sans Pro', sans-serif`,
  },
};

export { theme };
